<?php
/**
 * Action to browse server files
 * @author Sergii Gamaiunov <hello@webkadabra.com>
 */
class BrowseServerAction extends CAction
{
	/**
	 * 
	 */
	public $uploadPathAlias='frontend.www';
	
	/**
	 * 
	 */
	public function run()
	{
		if(empty($_POST) || !isset($_POST['dir'])) {
			Yii::app()->end();
		}
		
		$dir = urldecode($_POST['dir']);
		$root = Yii::getPathOfAlias($this->uploadPathAlias);

		$folder = $root . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR;
		
		if (file_exists($folder)) {
			$files = scandir($folder);
			natcasesort($files);
			if (count($files) > 2) { /* The 2 accounts for . and .. */
				echo "<ul class=\"jqueryFileTree\" style=\"display: none;\">";
				// All dirs
				foreach ($files as $file) {
					if (file_exists($folder . $file) && $file != '.' && $file != '..' && is_dir($folder . $file)) {
						echo "<li class=\"directory collapsed\"><a href=\"#\" rel=\"" . htmlentities($dir . $file) . "/\">" . htmlentities($file) . "</a></li>";
					}
				}
				// All files
				foreach ($files as $file) {
					if (file_exists($folder . $file) && $file != '.' && $file != '..' && !is_dir($folder . $file)) {
						$ext = preg_replace('/^.*\./', '', $file);
						echo "<li class=\"file ext_$ext\"><a href=\"#\" rel=\"" . htmlentities($dir . $file) . "\">" . htmlentities($file) . "</a></li>";
					}
				}
				echo "</ul>";
			}
		}
	}
}