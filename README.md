Webkadabra Ux Commons Widget
==========================
##### Author: Sergii Gamaiunov <hello@webkadabra.com>
##### [http://webkadabra.com](http://webkadabra.com)

Introduction
======================

This a simple helper for common UX interactions for regular Yii app


Dependancies
=======================

* jquery file tree (included)


Installation
=======================
1. Preparation
-----------------------

1.1. Add Core "wkd" path alias. Somewhere in your configs, add (adjust accordingly to where you have installed Webkadabra extensions):

	~~~
	Yii::setPathOfAlias('wkd', $root . DIRECTORY_SEPARATOR . 'common' . DIRECTORY_SEPARATOR . 'extensions'. DIRECTORY_SEPARATOR . 'wkd');
	~~~
	
1.2. Copy contents of this folder to /path/to/extensions/wkd/widgets/file-tree folder

1.3. Add this in your view (usually - in layout file):

	~~~
	<?php
	...
	$this->widget('wkd.widgets.file-tree.WkdFileTree', array(
		'target' => '#filtesTree',
		'options' => array(
			'root' => '/images/',
			'script' => '/site/browseFrontend',
		),
		'callback' => 'function(file) {
			file = "' . rtrim(app()->params['frontend.url'], '/') . '" + file;
			$("#filebrowse_preview").attr("src", file);
			$("#uploadInsertCode_browse").val("<img src=\""+file+"\" />");
			$("#filebrowse_preview").show();
			$("#server-filebrowser-controles").show();
		}'
	));
	echo CHtml::hiddenField("uploadInsertCode_browse", '', array("id"=>"uploadInsertCode_browse"));
	...
	~~~
	
1.3. Add server browser action to your desired controller (i.e. SiteController.php):

	~~~
	<?php
	...
	/**
	 *
	 * @return array actions
	 */
	public function actions()
	{
		return array(
			...
			'browseFrontend' => array(
				'class' => 'wkd.widgets.file-tree.BrowseServerAction',
			),
		);
	}
	...
	~~~

2. Usage
------------------------

See Pnotify documentation
