<?php
/**
 * WkdFileTree displays a file tree
 *
 * WkdFileTree encapsulates the {@link http://www.abeautifulsite.net/blog/2008/03/jquery-file-tree/} plugin.
 * 
 * @author Sergii Gamaiunov <hello@webkadabra.com>
 * @link http://www.webkadabra.com/
 * @copyright Copyright &copy; Webkadabra
 */
class WkdFileTree extends CWidget
{
	public $options;
	public $scriptUrl;
	public $target;
	public $callback;

	/**
	 * Run this widget.
	 */
	public function run()
	{
		$path = __DIR__ . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'jqueryFileTree';
		$this->scriptUrl = CHtml::asset($path);

		$cs = Yii::app()->getClientScript();

		$cs->registerScriptFile($this->scriptUrl . '/jqueryFileTree.js');
		$cs->registerCssFile($this->scriptUrl . '/jqueryFileTree.css');

		if (!isset($this->options['script'])) {
			$this->options['script'] = $this->scriptUrl . '/connectors/jqueryFileTree.php';
		}
		$options = CJavaScript::encode($this->options);


		if ($this->callback) {
			$js = "jQuery('{$this->target}').fileTree($options, {$this->callback});";
		} else {
			$js = "jQuery('{$this->target}').fileTree($options);";
		}


		$cs->registerScript(__CLASS__ . '#' . $this->target, $js);

	}
}